#!/bin/bash

if [ -n "$1" ]; then
    answer=$(zenity --title="Name me!" --text "Window group title" --entry)
else
    answer=$(zenity --title="Name me!" --text "Window title" --entry)
fi
if [ -n "$answer" ]; then
    if [ -n "$1" ]; then
        swaymsg 'focus parent, title_format "$answer", focus child'
    else
        swaymsg title_format "$answer"
    fi
else
    if [ -n "$1" ]; then
        swaymsg 'focus parent, title_format %title, focus child'
    else
        swaymsg title_format %title
    fi
fi
