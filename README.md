# Wayland Sway Dotfiles

My Arch Linux dotfiles for Wayland + Sway


### To clone inside non-empty $HOME
```shell
git init .
git remote add -t \* -f origin git@gitlab.com:andriperalt/wayland-sway-dotfiles.git
git checkout master
```

# Credits:
https://stackoverflow.com/questions/9864728/how-to-get-git-to-clone-into-current-directory
